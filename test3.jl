using Test

# Time tracking:
# Day3: 0h00

include("Day3.jl")

in1 = "#123 @ 3,2: 5x4"
expected1 = Day3.claim(123, 3, 2, 5, 4)
@test Day3.parse(in1) == expected1

@test Day3.findoverlapcount([
    "#1 @ 0,0: 1x1",
    "#2 @ 1,1: 1x1",
]; size=10) == 0

@test Day3.findoverlapcount([
    "#1 @ 0,0: 1x1",
    "#2 @ 0,0: 1x1",
]; size=10) == 1

@test Day3.findoverlapcount([
    "#1 @ 0,0: 2x1",
    "#2 @ 0,0: 2x1",
]; size=10) == 2

@test Day3.findoverlapcount([
    "#1 @ 0,0: 1x2",
    "#2 @ 0,0: 1x2",
]; size=10) == 2

@test Day3.findoverlapcount([
    "#1 @ 0,0: 2x2",
    "#2 @ 0,0: 2x2",
]; size=10) == 4

@test Day3.findoverlapcount([
    "#1 @ 0,0: 2x2",
    "#2 @ 1,1: 1x1",
]; size=10) == 1

@test Day3.findoverlapcount([
    "#1 @ 1,3: 4x4",
    "#2 @ 3,1: 4x4",
    "#3 @ 5,5: 2x2",
]; size=10, printfield=true) == 4

input3 = readlines("input3.txt")
@test Day3.findoverlapcount(input3) == 121163

@test Day3.findclear([
    "#1 @ 1,3: 4x4",
    "#2 @ 3,1: 4x4",
    "#3 @ 5,5: 2x2",
]; size=10, printfield=true) == Day3.claim(3, 5, 5, 2, 2)

@test Day3.findclear(input3) == Day3.claim(943, 235, 134, 16, 11)
