include("day1.jl")

function parse(filename)
    return sum(values)
end

values1 = readlines("input1.txt")
@info "Day1 resulting frequency", Day1.sum(values1)
@info "Day1 first duplicate frequency", Day1.finddupl(values1)
