using Test

# Time tracking:
# Day2: 0h43

include("Day2.jl")

function testday2(id, expected2, expected3)
    (count2, count3) = Day2.count(id)
    @test count2 == expected2
    @test count3 == expected3
end
testday2("abcdef", 0, 0)
testday2("bababc", 1, 1)
testday2("abbcde", 1, 0)
testday2("abcccd", 0, 1)
testday2("aabcdd", 1, 0)
testday2("abcdee", 1, 0)
testday2("ababab", 0, 1)

@test Day2.checksum(["abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab",]) == 12

input2 = readlines("input2.txt")
@test Day2.checksum(input2) == 4980

boxids = [
    "abcde",
    "fghij",
    "klmno",
    "pqrst",
    "fguij",
    "axcye",
    "wvxyz",
]
hits = ["fghij","fguij",]
@test Day2.findmatches(boxids) == hits
commons = ["fgij"]
@test Day2.findcommons(boxids) == commons

@test Day2.findmatches(input2) == ["qysdtrkloagnpfozuwujmhrbvx", "qysdtrkloagnxfozuwujmhrbvx",]
@test Day2.findcommons(input2) == ["qysdtrkloagnfozuwujmhrbvx"]
