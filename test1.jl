using Test

# Time tracking:
# Day1: 0h55

include("Day1.jl")

@test Day1.sum(["+1", "-2", "+3", "+1",]) == 3
@test Day1.sum(["+1", "+1", "+1",]) == 3
@test Day1.sum(["+1", "+1", "-2",]) == 0
@test Day1.sum(["-1", "-2", "-3",]) == -6

input1 = readlines("input1.txt")
input1ints = map(x->parse(Int, x), input1)
@test Day1.sum(input1ints) == 479

@test Day1.finddupl(["+1", "-2", "+3", "+1", "+1", "-2",]) == 2
@test Day1.finddupl([+1, -1,]) == 0
@test Day1.finddupl([+3, +3, +4, -2, -4]) == 10
@test Day1.finddupl([-6, +3, +8, +5, -6]) == 5
@test Day1.finddupl([+7, +7, -2, -7, -4]) == 14

@test Day1.finddupl(input1ints) == 66105
