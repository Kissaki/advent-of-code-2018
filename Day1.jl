module Day1

import Base.parse

function sum(values)
    ints = map(x->parse(Int, x), values)
    return sum(ints)
end

function sum(values::Vector{Int})
    freq = 0
    for value in values
        freq += value
    end
    return freq
end

function finddupl(values)
    ints = map(x->parse(Int, x), values)
    return finddupl(ints)
end

function finddupl(values::Vector{Int})
    freq = 0
    known = [0]
    while true
        for value in values
            freq += value
            if in(freq, known)
                @info "identified duplicate frequency $freq"
                return freq
            end
            push!(known, freq)
        end
    end
end

end
