module Day2

function count(id)
    data = Dict()
    for c in id
        if in(c, keys(data))
            data[c] = data[c] + 1
        else
            data[c] = 1
        end
    end
    has2 = false
    has3 = false
    for v in values(data)
        if v == 2
            has2 = true
        elseif v == 3
            has3 = true
        end
    end
    has2, has3
end

function checksum(values)
    count2 = 0
    count3 = 0
    for value in values
        has2, has3 = count(value)
        if has2
            count2 += 1
        end
        if has3
            count3 += 1
        end
    end
    return count2 * count3
end

function findmatches(values)
    matches = []
    for word1 in values
        for word2 in values
            if word1 == word2
                continue
            end

            if length(word1) != length(word2)
                @error "Unexpected unmatching id length for $word1 and $word2"
            end

            mismatchcount = 0
            for i = 1:length(word1)
                c1 = word1[i]
                c2 = word2[i]

                if c1 != c2
                    mismatchcount += 1
                end
            end
            if mismatchcount == 1
                @info "Identified $word1 $word2"
                push!(matches, word1)
                push!(matches, word2)
            end
        end
    end
    unique!(matches)
    @info "Found matches $matches"
    matches
end

function findcommons(values)
    commons = []
    for word1 in values
        for word2 in values
            if word1 == word2
                continue
            end

            if length(word1) != length(word2)
                @error "Unexpected unmatching id length for $word1 and $word2"
            end

            common = ""
            mismatchcount = 0
            for i = 1:length(word1)
                c1 = word1[i]
                c2 = word2[i]
                if c1 != c2
                    mismatchcount += 1
                else
                    common *= c1
                end
            end
            if mismatchcount == 1
                push!(commons, common)
            end
        end
    end
    unique!(commons)
    @info "Found commons $commons"
    commons
end

end
