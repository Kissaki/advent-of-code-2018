module Day3

import Base.parse

struct claim
    id::Int
    x::Int
    y::Int
    width::Int
    height::Int
end

function parse(line)
    # Example line: #123 @ 3,2: 5x4
    regex = r"^#(?<id>[0-9]+) @ (?<x>[0-9]+)\,(?<y>[0-9]+)\: (?<w>[0-9]+)x(?<h>[0-9]+)$"
    m = match(regex, line)
    claim(
        parse(Int, m[:id]),
        parse(Int, m[:x]),
        parse(Int, m[:y]),
        parse(Int, m[:w]),
        parse(Int, m[:h]),
    )
end

function findoverlapcount(lines; size=1000, printfield=false)
    field = Dict()
    for x in 1:size
        for y in 1:size
            field[(x,y)] = false
        end
    end
    @info "field length $(length(field))"
    claims = []
    for line in lines
        claim = parse(line)
        push!(claims, claim)
    end
    claimcount = length(claims)
    @info "Number of claims: $claimcount"
    for i in 1:claimcount
        for j in (i+1):claimcount
            c1 = claims[i]
            c2 = claims[j]
            minX = c2.x
            maxX = c2.x + c2.width - 1
            minY = c2.y
            maxY = c2.y + c2.height - 1
            for x1 in c1.x:c1.x+c1.width-1
                if x1 < minX || x1 > maxX
                    # c1 x outside x area of c2
                    continue
                end
                for y1 in c1.y:c1.y+c1.height-1
                    if y1 < minY || y1 > maxY
                        # c1 y outside y area of c2
                        continue
                    end
                    field[(x1,y1)] = true
                end
            end
        end
    end
    count = 0
    for x in values(field)
        if x
            count += 1
        end
    end
    @info "resulting count $count"
    if printfield
        for x in 1:size
            print("|")
            for y in 1:size
                print(field[(x,y)] ? "X" : "_")
            end
            println("|")
        end
    end
    count
end

function findclear(lines; size=1000, printfield=false)
    field = Dict()
    for x in 1:size
        for y in 1:size
            field[(x,y)] = false
        end
    end
    @info "field length $(length(field))"
    claims = []
    for line in lines
        claim = parse(line)
        push!(claims, claim)
    end
    claimcount = length(claims)
    @info "Number of claims: $claimcount"
    for i in 1:claimcount
        for j in (i+1):claimcount
            c1 = claims[i]
            c2 = claims[j]
            minX = c2.x
            maxX = c2.x + c2.width - 1
            minY = c2.y
            maxY = c2.y + c2.height - 1
            for x1 in c1.x:c1.x+c1.width-1
                if x1 < minX || x1 > maxX
                    # c1 x outside x area of c2
                    continue
                end
                for y1 in c1.y:c1.y+c1.height-1
                    if y1 < minY || y1 > maxY
                        # c1 y outside y area of c2
                        continue
                    end
                    field[(x1,y1)] = true
                end
            end
        end
    end
    for i in 1:length(claims)
        claim = claims[i]
        isclear = true
        for x in claim.x:claim.x+claim.width-1
            for y in claim.y:claim.y+claim.height-1
                if field[(x,y)]
                    isclear = false
                end
            end
        end
        if isclear
            return claim
        end
    end
    @info "resulting count $count"
    if printfield
        for x in 1:size
            print("|")
            for y in 1:size
                print(field[(x,y)] ? "X" : "_")
            end
            println("|")
        end
    end
end

end
